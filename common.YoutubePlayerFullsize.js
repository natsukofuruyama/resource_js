(function(root, $){


  /*----------------------------------------------------------------------
    @constructor
  ----------------------------------------------------------------------*/

  /**
   * YoutubePlayerFullsize実装
   *
   * @constructor
   * @class YoutubePlayerFullsize
   * @return {YoutubePlayerFullsize}
   */
  function YoutubePlayerFullsize($movie, $fullsize, options){

    this.props = $.extend(true, {}, {
      $movie     : $movie,
      $fullsize  : $fullsize,
      datumWidth : 1200,
      datumHeight: 0,
      ratioX     : 16,
      ratioY     : 9,
      resize     : false
    }, YoutubePlayerFullsize.defaluts, options);

    this.props.datumHeight = (this.props.datumWidth / this.props.ratioX) * this.props.ratioY;

    this._init();
  }

  var p = YoutubePlayerFullsize.prototype;


  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  /**
   * バージョン情報
   *
   * @static
   * @property VERSION
   * @type {String}
   */
    YoutubePlayerFullsize.VERSION = '1.0';


  /**
   * 定数
   *
   * @property p
   * @type {Object}
   */
    YoutubePlayerFullsize.defaluts = {
    };

  /**
   * プロトタイプオブジェクト
   *
   * @property p
   * @type {Object}
   */
    p = YoutubePlayerFullsize.prototype;

  /**
   * パラメータ
   *
   * @type {Object}
   */
    p.param = null;


  /**
   * クラスネーム
   *
   * @property name
   * @type {String}
   */
    p.name = 'YoutubePlayerFullsize';



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期実行
   */
  p._init = function(){
    var self = this;

    self._setSize();

    if (self.props.resize) {
      self.onResize();
    }
  };


  /**
   * リサイズイベントの追加
   */
  p.onResize = function(){
    var self = this;

    $(window).on('resize.YoutubePlayerFullsize', function() {
      self._setSize();
    });
  };


  /**
   * リサイズイベントの削除
   */
  p.offResize = function(){
    $(window).off('resize.YoutubePlayerFullsize');
  };



  /**
   * サイズのセット
   */
  p._setSize = function(){
    var self = this;
    var w = self.props.$fullsize.width(),
        h = self.props.$fullsize.height(),
        mw = w,
        mh = Math.round(self.props.datumHeight * (mw / self.props.datumWidth));

    if (mh < h) { //動画の高さが表示サイズの高さより小さい場合
      mh = h;
      mw = Math.round(self.props.datumWidth * (mh / self.props.datumHeight));
    }

    self.props.$movie.css({
      width: mw,
      height: mh,
      marginTop: (h - mh) / 2,
      marginLeft: (w - mw) / 2
    });
  };





  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.YoutubePlayerFullsize = YoutubePlayerFullsize;


}(window, jQuery));
