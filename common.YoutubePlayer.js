(function(root, $){


  /*----------------------------------------------------------------------
    @constructor
  ----------------------------------------------------------------------*/

  /**
   * YoutubePlayer実装
   *
   * @constructor
   * @class YoutubePlayer
   * @return {YoutubePlayer}
   */
  function YoutubePlayer($target, options, apiOptions){

    this.$target = $target;
    this.player = null;

    this.props = $.extend(true, {}, {
      loop                 : false,
      mute                 : false,
      autoplay             : false // 読み込み後すぐ再生 false:しない true:する
    }, YoutubePlayer.defaluts, options);

    this.apiOptions =  $.extend(true, {}, {
      width      : 610,
      height     : 390,
      videoId    : '',

      playerVars: {
        autoplay      : 1, // 自動再生  0:しない 1:する
        loop          : 0, // ループ  0:しない 1:する
        controls      : 1, // コントローラー  0:非表示 1:表示
        enablejsapi   : 1, // JavaScript API  0:無効 1:有効
        iv_load_policy: 3, // 動画アノテーション  1:表示 3:非表示
        disablekb     : 1, // キーボードで操作  0:有効 1:無効
        showinfo      : 0, // 動画のタイトルやアップロードしたユーザーなどの情報  0:非表示 1:表示
        rel           : 0, // 関連動画  0:非表示 1:表示
        start         : 0, // 動画の再生位置
        modestbranding: 1, // YouTubeロゴ 0:表示 1: 非表示,
        wmode         : "transparent" // z-indexを有効にする
      },
      events: {
        onReady                : $.noop, // 動画プレーヤーの準備完了後
        onPlaybackQualityChange: $.noop, // 画質切り替え時
        onStateChange          : $.noop, // プレーヤーの状態の変更時
        onError                : $.noop // エラー時
      }
    }, apiOptions);

    this._init();
  }

  var p = YoutubePlayer.prototype;


  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  /**
   * バージョン情報
   *
   * @static
   * @property VERSION
   * @type {String}
   */
    YoutubePlayer.VERSION = '1.0';


  /**
   * 定数
   *
   * @property p
   * @type {Object}
   */
    YoutubePlayer.defaluts = {
    };

  /**
   * プロトタイプオブジェクト
   *
   * @property p
   * @type {Object}
   */
    p = YoutubePlayer.prototype;

  /**
   * パラメータ
   *
   * @type {Object}
   */
    p.param = null;


  /**
   * クラスネーム
   *
   * @property name
   * @type {String}
   */
    p.name = 'YoutubePlayer';



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期実行
   */
  p._init = function(){
    var self = this;
    self.apiOptions.events = {
      onReady                : (function(event) { self._onPlayerReady(event); }), // 動画プレーヤーの準備完了後
      onPlaybackQualityChange: (function(event) { self._onPlayerPlaybackQualityChange(event); }), // 画質切り替え時
      onStateChange          : (function(event) { self._onPlayerStateChange(event); }), // プレーヤーの状態の変更時
      onError                : (function(event) { self._onPlayerError(event); }) // エラー時
    };
    self.player = new YT.Player(self.$target, self.apiOptions);
  };



  p.event = function(){
    var self = this;
  };


  p.getPlayer = function() {
    return this.player;
  };



  /**
   * 動画プレーヤーの準備完了後
   */
  p._onPlayerReady = function(event) {
    var self = this;

    if (self.props.mute) {
      event.target.mute();
      self.player.mute();
    }
    if (self.props.autoplay && !AMP.isSmartDevice()) {
      self.play();
    }
    self._onPlayerReadyCallback();
  };


  /**
   * 動画プレーヤーの準備完了後のコールバック
   */
  p._onPlayerReadyCallback = function() {
  };



  /**
   * 画質切り替え時
   */
  p._onPlayerPlaybackQualityChange = function(event) {
  };



  /**
   * プレーヤーの状態の変更時
   */
  p._onPlayerStateChange = function(event) {
    var self = this;
    if (event.data == YT.PlayerState.ENDED && self.props.loop) {
      self.play();
    }
  };


  /**
   * エラー時
   */
  p._onPlayerError = function(event) {
  };



  /**
   * スタート
   */
  p.play = function(){
    var self = this;

    self.player.playVideo();
  };


  /**
   * ポーズ
   */
  p.pause = function(){
    var self = this;

    self.player.pauseVideo();
  };


  /**
   * ストップ
   */
  p.stop = function(){
    var self = this;

    self.player.stopVideo();
  };


  /**
   * 削除
   */
  p.destroy = function(){
    var self = this;

    self.player.destroy();
  };


  /**
   * ポーズ 再生切り替え
   */
  p.pauseSwitch = function(){
    var self = this;

    switch(player.getPlayerState()) {
      case 1:
        self.pause();
        break;
      default:
        self.play();
    }
  };





  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.YoutubePlayer = YoutubePlayer;


}(window, jQuery));
