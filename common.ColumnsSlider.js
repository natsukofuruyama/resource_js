
(function(root, $){


  /**
   *  ２列～の表示スライダー
   *
   * @class ColumnsSlider
   * @constructor
   * @param {jQuery} $slider
   * @param {Object} options
   */
  function ColumnsSlider($slider, options) {
    this.props = $.extend(
      true,
      {},
      this.props,
      ColumnsSlider.OPTIONS,
      {
        $window     : $(window),
        $slider     : $slider.find('.slider'),
        $stage      : $slider.find('.stage'),
        $list       : $slider.find('.list'),
        $item       : $slider.find('.list').children(),
        $prev       : $slider.find('.prev'),
        $next       : $slider.find('.next'),
        $pointer    : $slider.find('.c-pointer'),
        $point      : $slider.find('.c-pointer').find('li'),
        itemLength  : 0,
        next        : 0,
        current     : 0,
        timer       : 5000,
        timerId     : null,
        ACTIVE_CLASS: 'is-active',
        HIDDEN_CLASS: 'is-hide',
        column      : 2,
        resize      : false,

        slideMax    : 0, //スライドできる最大値

        ease        : 'easeOutExpo',
        duration    : 600,
        _isAnime    : false
      },
      options
    );

    this.props.itemLength = this.props.$item.length;
  }

  // prototype
  var p = ColumnsSlider.prototype;



  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  p.className = 'ColumnsSlider';

  /**
   * デフォルト値オブジェクト
   * コンストラクタが呼び出し時に、引数とoptionsをmixinしてpropsオブジェクトに格納します
   *
   * @static
   * @property OPTIONS
   * @type {Object}
   */
  ColumnsSlider.OPTIONS = {
  };



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   *
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function(){
    var self = this;
    self.props.$stage.imagesLoaded(function(){
      self.set();
    });
  };


  /*--------------------------------------------------------------------------
     Setting
  ---------------------------------------------------------------------------*/

  /**
   * 初期設定
   *
   * @set
   * @method set
   * @return {Void}
   */
  p.set = function(){
    var self = this;

    self.props.current = self.getCurrentPosition(self.props.current, 1, self.props.column);
    self.setStyle();
    self.setPoint();
    self.setPagination();
    self.setTimer();
    self.event();
    self.flickEvent();

    if (self.props.resize) {
      self.resizeEvent();
    }
  };

  /**
   * ポインターセット
   *
   * @setPoint
   * @method setPoint
   * @return {Void}
   */
  p.setPoint = function(){
    var self = this;

    self.props.$point.removeClass(self.props.ACTIVE_CLASS);

    var $point = self.props.$pointer.children().eq(0).clone(true),
        i = 0,
        $clone = '';

    self.props.$pointer.children().remove();
    for(; i < self.props.slideMax; i++){
      self.props.$pointer.append($point.clone(true));
    }
    self.props.$point = self.props.$pointer.find('li a');
    self.props.$point.eq(self.props.current).addClass(self.props.ACTIVE_CLASS);
  };


  /**
   * prev next表示切り替え
   * @method setPagination
   * @return {Void}
   */
  p.setPagination = function() {
    var self = this;
    if (self.props.slideMax <= 1) {
      self.props.$prev.addClass(self.props.HIDDEN_CLASS);
      self.props.$next.addClass(self.props.HIDDEN_CLASS);
    } else {
      self.props.$prev.removeClass(self.props.HIDDEN_CLASS);
      self.props.$next.removeClass(self.props.HIDDEN_CLASS);
    }
  };


  /**
   * アイテムクローン
   *
   * @setItemClone
   * @method setItemClone
   * @return {Void}
   */
  p.setItemClone = function(){
    var self = this;
    var i = 0,
        c = '';
    for(; i < self.props.CLONE; i++){
      c += self.props.$list.html();
    }
    self.props.$list.html(c);
    self.props.$item = self.props.$list.children();
  };


  /**
   * 初期スタイル
   *
   * @setStyle
   * @method setStyle
   * @return {Void}
   */
  p.setStyle = function(){
    var self = this;

    var width = self.getColumnWidth();
    self.props.slideMax = self.getSlideMax(self.props.current);

    self.props.$item.css({
      width: width
    });
    self.props.$list.css({
      width: width * self.props.itemLength,
      left: self.props.$stage.width() * self.props.current * -1
    });
    self.props.$stage.css({
      height: self.props.$item.height()
    });
  };


  /**
   * リサイズ時のスタイル設定
   * @method resizeStyle
   * @return {[type]} [description]
   */
  p.resizeStyle = function() {
    var self = this;

    clearTimeout(self.props.timerId);

    var width = self.getColumnWidth();

    self.props.$item.css({
      width: width
    });
    self.props.$list.css({
      width: width * self.props.itemLength,
      left: self.props.$stage.width() * self.props.current * -1
    });
    self.props.$stage.css({
      height: self.props.$item.height()
    });

    self.setTimer();
  };



  /**
   * スライドの初期化
   * @resetSlide
   * @return {[type]} [description]
   */
  p.resetSlide = function(column) {
    clearTimeout(this.props.timerId);
    this.props.current = this.getCurrentPosition(this.props.current, this.props.column, column);
    this.setColumn(column);
    this.setStyle();
    this.setPoint();
    this.setPagination();
    this.setTimer();
  };



  /**
   * カラム数のセット
   * @setColumn
   * @param {Number} column カラム数
   */
  p.setColumn = function(column) {
    this.props.column = column;
    return column;
  };


  /**
   * スライドできる最大値を変えします
   * @getSlideMax
   * @return {Number}  スライドできる最大値
   */
  p.getSlideMax = function() {
    return Math.ceil(this.props.itemLength / this.props.column);
  };


  /**
   * 現在の位置の位置を今のカラム数の場合におきかえて、
   * その位置を返します
   * @getCurrentPosition
   * @param  {Numvber} current 現在の位置
   * @return {Number}          今のカラム数の時のスライド位置
   */
  p.getCurrentPosition = function(index, prevColumn, nextColumn) {
    var current = prevColumn * index;
    return Math.floor(current / nextColumn);
  };


  /**
   * アイテムに適応させる横幅を
   * @getColumnWidth
   * this.props.$stageの横幅を基準に計算し返します
   * @return {Number} 横幅
   */
  p.getColumnWidth = function() {
    return this.props.$stage.width() / this.props.column;
  };


  /**
   * タイマーセット
   *
   * @setTimer
   * @method setTimer
   * @return {Void}
   */
  p.setTimer = function(){
    var self = this;
    clearTimeout(self.props.timerId);
    if(self.props.timer){
      self.props.timerId = setTimeout(function(){
        self.nextEvent();
      }, self.props.timer);
    }
  };



  /*--------------------------------------------------------------------------
     Event
  ---------------------------------------------------------------------------*/

  /**
   * イベント
   *
   * @event
   * @method event
   * @return {Void}
   */
  p.event = function(){
    var self = this;

    // prev
    self.props.$prev.find('a').click(function(){
      if(self.props._isAnime) return false;
      self.prevEvent();
      return false;
    });

    // next
    self.props.$next.find('a').click(function(){
      if(self.props._isAnime) return false;
      self.nextEvent();
      return false;
    });

    // pointer
    self.props.$pointer.find('a').click(function(){
      if(self.props._isAnime) return false;
      self.pointerEvent(self.props.$pointer.find('a').index(this));
      return false;
    });

    // hover
    self.props.$stage.hover(
      function(){
        clearTimeout(self.props.timerId);
      },
      function(){
        self.setTimer();
      });
  };

  /**
   * リサイズイベント
   *
   * @resizeEvent
   * @method resizeEvent
   * @return {Void}
   */
  p.resizeEvent = function() {
    var self = this;

    self.props.$window.on('resizestop', function() {
      self.resizeStyle();
    });
  };

  /**
   * PREVイベント
   *
   * @prevEvent
   * @method prevEvent
   * @return {Void}
   */
  p.prevEvent = function(){
    var self = this;
    self.viewStart(self.getNextCount(self.props.current - 1));
  };

  /**
   * NEXTイベント
   *
   * @nextEvent
   * @method nextEvent
   * @return {Void}
   */
  p.nextEvent = function(){
    var self = this;
    self.viewStart(self.getNextCount(self.props.current + 1));
  };

  /**
   * ポインターイベント
   *
   * @pointerEvent
   * @method pointerEvent
   * @return {Void}
   */
  p.pointerEvent = function(index){
    var self = this;
    self.viewStart(self.getNextCount(index));
  };

  /**
   * フリックイベント
   *
   * @flickEvent
   * @method flickEvent
   * @return {Void}
   */
  p.flickEvent = function(){
    var self = this;

    self.props.$stage
    .on('flickmoveX', function(event){
      if(self.props._isAnime) return false;
      clearTimeout(self.props.timerId);
      self.props.$list.css({marginLeft: event.moveX});
    })
    .on('flickcancel', function(event){
      if(self.props._isAnime) return false;
      self.props.$list.stop(true, false).velocity({marginLeft: 0}, self.props.duration, self.props.ease,
        function(){
          self.setTimer();
        });
    })
    .on('flick', function(event){
      if (event.moveX < 0 && !self.props._isAnime) {
        self.nextEvent();
      }
      else if (0 < event.moveX && !self.props._isAnime) {
        self.prevEvent();
      }
      else if (!self.props._isAnime) {
        self.props.$list.stop(true, false).velocity({marginLeft: 0}, self.props.duration, self.props.ease,
          function(){
            self.setTimer();
          });
      }
    });
  };



  /*--------------------------------------------------------------------------
     Control
  ---------------------------------------------------------------------------*/

  /**
   * nextカウント
   *
   * @getNextCount
   * @method getNextCount
   * @return {Void}
   */
  p.getNextCount = function(next){
    var self = this;
    if(next == self.props.slideMax){
      next = 0;
    } else if (next < 0) {
      next = self.props.slideMax -1;
    }
    return next;
  };

  /**
   * ビューに投げる前
   *
   * @viewStart
   * @method viewStart
   * @return {Void}
   */
  p.viewStart = function(next){
    var self = this;
    clearTimeout(self.props.timerId);
    self.props._isAnime = true;
    self.pointer(self.props.current, next);
    self.tween(next);
  };



  /*--------------------------------------------------------------------------
     View
  ---------------------------------------------------------------------------*/

  /**
   * ポインターチェンジ
   *
   * @pointer
   * @method pointer
   * @return {Void}
   */
  p.pointer = function(current, next){
    var self = this;
    self.props.$point.eq(current).removeClass(self.props.ACTIVE_CLASS);
    self.props.$point.eq(next).addClass(self.props.ACTIVE_CLASS);
  };


  /**
   * スライドアニメーション
   *
   * @tween
   * @method tween
   * @return {Void}
   */
  p.tween = function(next){
    var self = this;
    self.props.$list
      .stop(true, false)
      .velocity({
        left: self.props.$stage.width() * next * -1,
        marginLeft: 0
      }, self.props.duration, self.props.ease,
        function(){
          self.props.current = next;
          self.props._isAnime = false;
          self.setTimer();
        });
  };



  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.ColumnsSlider = ColumnsSlider;

}(this, jQuery));
