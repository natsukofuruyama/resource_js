(function(root, $){


  /*--------------------------------------------------------------------------
    common.Tab
  --------------------------------------------------------------------------*/

  function Tab($target, options){
    this.props = $.extend(true, {
      $navi      : $target.find('.tab-nav'),
      $trigger   : $target.find('.tab-nav a'),
      $content   : $target.find('.tab-contents'),
      $section   : $target.find('.tab-section'),
      $element   : $target.find('.tab-element'),
      $prev      : $target.find('.prev a'),
      $next      : $target.find('.next a'),
      length     : $target.find('.tab-section').length,

      current    : 0,
      activeClass: 'is-active',
      dur        : 450,
      ease       : 'easeOutQuad',
      isAnime    : false
    }, options);

    this._init();
  }

  // prototype
  var p = Tab.prototype;


  /**
   * 初期化
   */
  p._init = function(){
    this._set();
    this._event();
  };


  /**
   * 初期設定
   */
  p._set = function(){
    var self = this;

    self.navActive(self.props.current);
    self.show(self.props.current);
  };


  /**
   * 初期化
   * @param  {Number} current 表示させる番号
   */
  p.reset = function(current) {
    var self = this;

    var i = 0;
    for (; i < self.props.length; i += 1) {
      self.hide(i);
    }
    self.props.$section.velocity('stop');
    self.current = current;
    self.navActive(current);
    self.show(current);
    self.props.isAnime = false;
  };


  /**
   * イベント
   */
  p._event = function() {
    var self = this;

    self.props.$navi.find('a').on('click.commonTab', function() {
      var index = $(this).closest('li').index();
      if (index != self.props.current && !self.props.isAnime) {
        self.controller(self.props.current, index);
      }
      return false;
    });
  };


  /**
   * ナビのアクティブ処理
   * @props  {Number} next アクティブにする番号
   */
  p.navActive = function(next) {
    var self = this;
    self.props.$trigger.removeClass(self.props.activeClass);
    self.props.$trigger.eq(next).addClass(self.props.activeClass);
  };


  /**
   * 非表示
   * @props  {Number} current 非表示にする番号
   */
  p.hide = function(current) {
    var self = this;

    // self.props.$section.eq(current)
    //   .css({zIndex: 5})
    //   .velocity('stop')
    //   .velocity({
    //     opacity: 0
    //   }, self.props.dur, self.props.ease,
    //     function() {
          self.props.$section.eq(current).removeClass(self.props.activeClass).removeAttr('style');
        // });
  };


  /**
   * 表示
   * @props  {Number} next 表示させる番号
   */
  p.show = function(next) {
    var self = this;
    self.props.isAnime = true;

    self.props.$section.eq(next)
      .addClass(self.props.activeClass)
      .css({
        opacity: 0
      })
      .velocity('stop')
      .velocity({
        opacity: 1
      }, self.props.dur, self.props.ease,
        function() {
          self.props.$section.eq(next).removeAttr('style');
          self.props.isAnime = false;
        });
  };


  /**
   * コントローラー
   * @props  {Number}   current 今表示されている番号
   * @props  {Number}   next    次に表示させたい番号
   */
  p.controller = function(current, next) {
    var self = this;
    self.navActive(next);
    self.hide(current);
    self.show(next);
    self.props.current = next;
  };



  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.Tab = Tab;


}(window, jQuery));
