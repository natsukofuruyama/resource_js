
(function(root, $){

  /*--------------------------------------------------------------------------
    config
  --------------------------------------------------------------------------*/

    var $window = $(window),
        $head = $('head'),
        $html = $('html'),
        $body = $('body');


  /*--------------------------------------------------------------------------
    @utils
  --------------------------------------------------------------------------*/

  /**
   * @module util
   */
  var util = {};

  util.mediaChange = function(callback){
    var state = util.isOrientation();

    $(window).on('resize.mediaChange', function(){
      if(state !== util.isOrientation()){
        if (callback) callback();
        state = util.isOrientation();
      }
    }).trigger('resize.mediaChange');
  };

  util.isOrientation = function(){
    var w = $window.width(),
        h = $window.height();
    return w < h ? 'portrait' : 'landscape';
  };

  util.isDisplayType = (function(){
    return function(type){
      return $head.css('fontFamily') === type;
    };
  }());


  /**
   * fixedの横スクロール時の固定
   * @param  Object $target     対象のオブジェクト
   * @param  Number o_adjust    固定調整位置
   * @param  String o_direction 右側固定: 'right' 左固定: 'left' 省略で左固定
   */
  util.fixedX = function($target, o_adjust, o_direction){
    o_adjust = o_adjust === void 0 ? 0 : o_adjust;
    o_direction = o_direction === void 0 ? 'left' : o_direction;

    $window.on('scroll.fixedX, resize.fixedX', function() {
      if(o_direction === 'left'){
        $target.css({left: - $window.scrollLeft() + o_adjust});
      } else if (o_direction === 'right'){
        $target.css({
          right: ($body.width() - $window.width() - $window.scrollLeft() + o_adjust) * -1,
        });
      }
    });
  };


  /**
   * ほかページの要素を取得し、対象のエリアに吐き出します
   * @param  Object $target data属性を持つObject タグの挿入先
   * @param  Object getDom  ajax通信で取得したhtmlから挿入したいエレメントのID
   */
  util.appendContents = function($target, getDom, callback){
    var url = $target.data('get-table') +' '+ getDom;
    $target.load(url, function(data, state) {
      if (state === 'error') {
        $target.html('表の読み込みに失敗しました。再度ページを更新してください。');
      } else {
        if (_.isFunction(callback)) {
          callback();
        }
      }
    });
  };


  /**
   * 横幅に合わせて要素を100%としたスタイルを設定します
   * @param  Object $target 対象の要素
   * @param  Object getDom  対象の要素をwarpする要素
   */
  util.scaleSize = function($target, $wrap, adjust){
    if (!_.isNumber(adjust)) {
      adjust = 40;
    }
    var w = $target.width(),
        h = $target.height(),
        scale = ($window.width() - adjust) / w,
        heightPercent = (h - h * scale) / 2 * -1,
        widthPercent = (w - w * scale) / 2 * -1;

      $wrap.velocity({
        scale: scale,
        marginTop: heightPercent,
        marginBottom: heightPercent,
        marginLeft: widthPercent,
        marginRight: widthPercent
      }, 0);
  };


  /**
   * アクティブクラス付与
   * @param  Object $target アクティブクラスを振る要素
   */
  util.active = function($target){
    $target.addClass('is-active');
  };




  /**
   * セッションストレージの引数に与えたキーと値を保存
   */
  util.setSessionStorage = function(key, value) {
    return sessionStorage.setItem(key, value);
  };

  /**
   * セッションストレージの引数に与えたキーを返します
   */
  util.getSessionStorage = function(key) {
    return sessionStorage.getItem(key);
  };

  /**
   * セッションストレージの引数に与えたキーを削除します
   */
  util.removeSessionStorage = function() {
    return sessionStorage.removeItem(key);
  };

  /**
   * セッションストレージのデータをすべて削除
   */
  util.clearSessionStorage = function() {
    return sessionStorage.clear();
  };


  /**
   * youtube iframeAPIの読み込み後実行
   * scriptタグの挿入
   */
  util.youtubeReady = function(callback) {
    var EVENT_READY = "youtubeready";

    if(window.YT && window.YT.Player){
      return callback();
    }

    $(window).on(EVENT_READY, callback);

    window.onYouTubeIframeAPIReady = function(){
        $(this).trigger(EVENT_READY);
        window.onYouTubeIframeAPIReady = void 0;
    };
    // $("<script>", {src: "//www.youtube.com/iframe_api"}).appendTo("body"); macでバグ
    // $("<script>", {src: "https://www.youtube.com/iframe_api"}).appendTo("body");
  };



  /**
   * queryの取得
   */
  util.getQuery = function() {
    var vars = [],
        max = 0,
        hash = "",
        array = "";
    var url = window.location.search;

    if (!url) {
      return undefined;
    }

    hash = url.slice(1).split('&');
    max = hash.length;
    for (var i = 0; i < max; i += 1) {
      array = hash[i].split('=');
      vars.push(array[0]);
      vars[array[0]] = array[1];
    }

    return vars;
  };


  /**
   * queryを文字列で取得
   */
  util.getQueryString = function() {
    var url = window.location.search.slice(1);
    return url.length <= 0 ? undefined : url;
  };



  /**
   * queryの指定したキーの値を取得
   */
  util.getQueryVariable = function(variable) {
    var vars = common.util.getQuery();
    return vars[variable];
  };


  /**
   * URLの取得
   */
  util.getUrl = function() {
    return window.location.href;
  };

  /**
   * URLの取得
   */
  util.getUrlDeleteQuery = function () {
    var href = window.location.href;
    return window.location.href.substr(0, href.indexOf('?'));
  };

  util.getDomain = function () {
    return document.domain;
  };


  /**
   * 画像読み込み
   * @param  {Arrey} imgs イメージパスの配列
   */
  util.preloadImg = function(imgs){
    for (var i = 0; i < imgs.length; i++) {
      var img = new Image();
      img.src = imgs[i];
    }
    return imgs;
  };


  /**
   * 現在の時間をかえします
   * @return Number 現在の時間を返します
   */
  util.getCurrentTime = function() {
    if (Date.now === 'undefind') {
      return (new Date()).getTime();
    } else {
      return Date.now();
    }
  };



  /*--------------------------------------------------------------------------
   export
   --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.util = util;

}(this, jQuery));
