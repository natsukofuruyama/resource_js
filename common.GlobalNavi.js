
(function(root, $){



/*--------------------------------------------------------------------------
  @class GlobalNavi
--------------------------------------------------------------------------*/

common.GlobalNavi = (function() {

  function GlobalNavi($navi) {
    this.props = {
      $window         : $(window),
      $navi           : $navi,
      $gnav           : $navi.find('.hd-global-nav'),
      $dropdownTrigger: $navi.find('.hd-global-nav').find('.js-has-lower'),
      $openTrigger    : $navi.find('.btn-menu'),
      $minimumTrigger : $navi.find('.hd-minimum .menu'),
      $search         : $navi.find('.search'),
      $spItems        : null,

      minimumPositon  : 400,
      scrollCount     : 0,
      openPosition    : 0,
      MINIMUM_CLASS   : 'is-minimum',
      OPEN_CLASS      : 'is-open',
      HOVER_CLASS     : 'is-gnav-open',
      _isOpen         : false,
      _isMinimum      : false,
      _isScrollToggle : false
    };

    this.props.$spItems = this.props.$gnav.find('.search, .links > li, .traffic-links li');

    this._init();
  }

  /**
   * @type {prototype}
   */
  var p = GlobalNavi.prototype;

  /**
   * 初期化
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function() {
    this._event();
  };



  /**
   * イベント
   * @private
   * @method _event
   * @return {Void}
   */
  p._event = function() {
    var self = this;

    self.props.$search.find('.trigger').on('click.GlobalNavi', function() {
      self.props.$search.toggleClass(self.props.OPEN_CLASS);
      self.props.$search.find('.text').focus();
      return false;
    });

    self.props.$window.on('resize.GlobalNavi', function() {
      if (self.props._isOpen && AMP.mediaquery.mediaStyle == 'sp') {
        self.props.$gnav.height(self.props.$window.height());
      }
    });

    // メディアクエリ
    var changeMediaquery = function(mq) {
      self.reset();

      if (mq === 'sp' || AMP.isTablet()) {
        self.offEventPC();
        self.onEventSP();
      }
      else if (mq === 'pc') {
        self.offEventSP();
        self.onEventPC();
      }
    };
    AMP.mediaquery.on('change.globalNavi', function(mediaEvent){
      changeMediaquery(mediaEvent.mediaStyle);
    });
    changeMediaquery(AMP.mediaquery.mediaStyle);
  };


  /**
   * スマホイベントの追加
   * @method onEventSP
   * @return {Void}
   */
  p.onEventSP = function() {
    var self = this;

    self.props.$openTrigger.find('a').on('click.GlobalNaviSP', function() {
      self._controller(self.props._isOpen, 'openSP');
      return false;
    });
  };


  /**
   * スマホイベントの削除
   * @method offEventSP
   * @return {Void}
   */
  p.offEventSP = function() {
    var self = this;

    self.props.$spItems.removeAttr('style');
    self.props.$openTrigger.find('a').off('.GlobalNaviSP');
  };


  /**
   * PCイベントの追加
   * @method onEventPC
   * @return {Void}
   */
  p.onEventPC = function() {
    var self = this;

    self.props.$dropdownTrigger
    .on('mouseenter.GlobalNaviPC', function() {
      self.props.$navi.addClass(self.props.HOVER_CLASS);
      $(this).find('.hd-gnav-lower')
        .velocity('stop')
        .height('auto')
        .velocity({
          opacity: 1
        }, 300, 'easeOutExpo');
    })
    .on('mouseleave.GlobalNaviPC', function() {
      self.props.$navi.removeClass(self.props.HOVER_CLASS);
      $(this).find('.hd-gnav-lower')
        .velocity('stop')
        .velocity({
          opacity: 0
        }, 300, 'easeOutExpo',
          function(){
            $(this).removeAttr('style');
          });
    });
  };


  /**
   * PCイベントの削除
   * @method offEventPC
   * @return {Void}
   */
  p.offEventPC = function() {
    var self = this;

    self.props.$dropdownTrigger.off('.GlobalNaviPC');
  };


  /**
   * イベント
   * @private
   * @method minimumEvent
   * @return {Void}
   */
  p.minimumEvent = function() {
    var self = this;

    self.props.$minimumTrigger.on('click.GlobalNavi', function() {
      self._controller(self.props._isMinimum, 'minimum');
      return false;
    });

    self.props.$window.on('scroll.GlobalNavi', function() {
      var scrollTop = self.props.$window.scrollTop();

      if (self.props.minimumPositon < scrollTop && self.props._isScrollToggle && !self.props._isMinimum) {
        if (self._scrollCount()) {
          self.minimum();
        }
      }
      else if (scrollTop < self.props.minimumPositon && self.props._isScrollToggle) {
        self.maximum();
        self.props._isScrollToggle = false;
      }
      else if (self.props.minimumPositon < scrollTop && !self.props._isScrollToggle) {
        self.minimum();
        self.props._isScrollToggle = true;
      }
    });
  };


  /**
   * コントローラー
   * @private
   * @method _controller
   * @param  {Boolean} isOpen ナビを開くか
   * @return {Void}
   */
  p._controller = function(status, target) {
    if (target === 'openSP') {
      if (status) {
        this.closeSP();
      } else {
        this.openSP();
      }
    }
    else if (target === 'openPC') {
      if (status) {
        this.closePC();
      } else {
        this.openPC();
      }
    }
    else if (target === 'minimum') {
      if (status) {
        this.maximum();
      } else {
        this.minimum();
      }
    }
  };


  /**
   * スクロールのカウント
   * @return {Boolean} 恷弌晒できればtrue そうでなければfalse
   */
  p._scrollCount = function() {
    var y = Math.abs(this.props.openPosition - this.props.$window.scrollTop());
    this.props.scrollCount = y;

    if (this.props.minimumPositon <= this.props.scrollCount) {
      this.props.scrollCount = 0;
      return true;
    } else {
      return false;
    }
  };

  /**
   * 開く SP
   * @method openSP
   * @return {GlobalNavi}
   */
  p.openSP = function() {
    var self = this;

    self.props._isOpen = true;
    self.props.$openTrigger.addClass(self.props.OPEN_CLASS);
    self.props.$spItems
      .velocity('stop')
      .velocity({
        translateY: 40,
        opacity: 0
      }, 0);

    self.props.$navi.addClass(self.props.OPEN_CLASS);
    self.props.$gnav
      .velocity('stop')
      .velocity({
        opacity: 1
      }, 200, 'easeOutQuad');

    self.props.$spItems.each(function(i) {
      $(this)
        .velocity({
          translateY: 0,
          opacity: 1
        }, {
          duration: 300,
          delay: i * 50,
          easing: 'easeOutBack'
        });
    });
  };

  /**
   * 閉じる SP
   * @method closeSP
   * @return {GlobalNavi}
   */
  p.closeSP = function() {
    var self = this;

    self.props._isOpen = false;
    self.props.$openTrigger.removeClass(self.props.OPEN_CLASS);
    self.props.$gnav
      .velocity('stop')
      .velocity({
        opacity: 0
      }, 200, 'easeOutQuad',
        function() {
          self.props.$gnav.removeAttr('style');
          self.props.$navi.removeClass(self.props.OPEN_CLASS);
          self.props.$gnav.height(self.props.$window.height(0));
        });
  };

  /**
   * リセット SP
   * @method resetSP
   * @return {GlobalNavi}
   */
  p.resetSP = function() {
    var self = this;

    self.props._isOpen = false;
    self.props.$openTrigger.removeClass(self.props.OPEN_CLASS);
    self.props.$gnav.removeAttr('style');
    self.props.$navi.removeClass(self.props.OPEN_CLASS);
  };

  /**
   * 開く PC
   * @method openPC
   * @return {GlobalNavi}
   */
  p.openPC = function() {
    var self = this;

  };

  /**
   * 閉じる PC
   * @method closePC
   * @return {GlobalNavi}
   */
  p.closePC = function() {
    var self = this;

  };

  /**
   * リセット PC
   * @method resetPC
   * @return {GlobalNavi}
   */
  p.resetPC = function() {
    var self = this;

  };

  /**
   * 最小化
   * @method minimum
   * @return {GlobalNavi}
   */
  p.minimum = function() {
    this.props.openPosition = 0;
    this.props.$navi.addClass(this.props.MINIMUM_CLASS);
    this.props._isMinimum = true;
  };

  /**
   * 最大化
   * @method maximum
   * @return {GlobalNavi}
   */
  p.maximum = function() {
    this.props.openPosition = this.props.$window.scrollTop();
    this.props.$navi.removeClass(this.props.MINIMUM_CLASS);
    this.props._isMinimum = false;
  };

  /**
   * リセット
   * @method reset
   * @return {GlobalNavi}
   */
  p.reset = function() {
    this.resetPC();
    this.resetSP();
  };

  // return class
  return GlobalNavi;
}());




  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/


}(this, jQuery));
