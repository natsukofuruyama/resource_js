
(function(root, $){



/*--------------------------------------------------------------------------
  @class SideNavi
--------------------------------------------------------------------------*/

common.SideNavi = (function() {

  function SideNavi($side) {
    this.props = {
      $window : $(window),
      $content: $('#content-inline > .c-l-content'),
      $side   : $side,
      $snav   : $side.find('.side-nav'),
      $sBnrs  : $side.find('.side-bnrs'),
      $main   : $('#Main'),
      adjust  : 125 // 格納ナビの高さ
    };

    this._init();
  }

  /**
   * @type {prototype}
   */
  var p = SideNavi.prototype;


  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function() {
    this._mediaqueryChangeEvent();
  };



  /*--------------------------------------------------------------------------
     Setting
  ---------------------------------------------------------------------------*/

  /**
   * 初期設定
   */
  p._set = function() {
    this._setMinHeight();
    this._setPosition();
    this._onEvent();
  };


  /**
   * 初期化
   */
  p._reset = function() {
    this._offEvent();
    this.props.$side.css({
      minHeight: 0
    });
    this.props.$side.css({
      position: 'static',
      marginLeft: 0,
      bottom: 0
    });
  };



  /*--------------------------------------------------------------------------
     Event
  ---------------------------------------------------------------------------*/

  /**
   * ディアクエリ切り替わりイベント
   */
  p._mediaqueryChangeEvent = function() {
    var self = this;

    var changeMediaquery = function(mq) {
      if (mq === 'sp') {
        self._reset();
      }
      else if (mq === 'pc') {
        self._set();
      }
    };

    AMP.mediaquery.on('change.global', function(mediaEvent){
      changeMediaquery(mediaEvent.mediaStyle);
    });
    changeMediaquery(AMP.mediaquery.mediaStyle);
  };


  /**
   * イベントの追加
   */
  p._onEvent = function() {
    var self = this;

    self.props.$window.on('scroll.SideNavi resize.SideNavi', function() {
      self._setPosition();
    });

    self.props.$window.on('resize.SideNavi', function() {
      self._setMinHeight();
    });
  };


  /**
   * イベントの削除
   */
  p._offEvent = function() {
    var self = this;
    self.props.$window.off('scroll.SideNavi resize.SideNavi');
  };



  /*--------------------------------------------------------------------------
     View
  ---------------------------------------------------------------------------*/

  /**
   * 最小高をセット
   */
  p._setMinHeight = function() {
    var self = this;

    var h = self.props.$main.height() < self.props.$window.height() ? 0 : self.props.$window.height();
    self.props.$side.css({
      minHeight: h
    });
  };



  /**
   * 位置をセット
   */
  p._setPosition = function() {
    var self = this;

    var windowHeight = self.props.$window.height(),
        sideHeight = self.props.$snav.outerHeight(true) + self.props.$sBnrs.outerHeight(true),
        // sideHeight = self.props.$side.height(),
        mainHeight = self.props.$main.height(),
        contentHeight = self.props.$content.height(),
        contentOffsetTop = self.props.$content.offset().top - self.props.adjust,
        scrollY = self.props.$window.scrollTop(),
        scrollX = self.props.$window.scrollLeft(),
        sideEndPosition = self.props.$main.offset().top + sideHeight - windowHeight,
        onTheFixedPosition = contentHeight + contentOffsetTop - sideHeight;
        lowerFixingPosition = contentHeight + self.props.$content.offset().top - windowHeight;

    // sideの高さ < ウィンドウの高さ
    if (sideHeight < windowHeight - self.props.adjust) {
      // 上固定
      if (contentOffsetTop < scrollY && scrollY < onTheFixedPosition) {
        self.props.$side.css({
          position: 'fixed',
          marginLeft: scrollX * -1,
          top: self.props.adjust,
          bottom: 'auto'
        });
      }
      // 下固定
      else if (onTheFixedPosition < scrollY) {
        self.props.$side.css({
          position: 'absolute',
          marginLeft: 0,
          top: 'auto',
          bottom: 0
        });
      }
      // 通常
      else {
        self.props.$side.css({
          position: 'static',
          marginLeft: 0,
          bottom: 0
        });
      }
    }

    // ウィンドウの高さ < sideの高さ
    else {
      // 下固定位置を超えた時
      if (lowerFixingPosition < scrollY && sideHeight < contentHeight) {
        self.props.$side.css({
          position: 'fixed',
          marginLeft: scrollX * -1,
          bottom: scrollY - lowerFixingPosition
        });
      }
      // 下付き固定
      else if (sideEndPosition < scrollY && sideHeight < contentHeight) {
        self.props.$side.css({
          position: 'fixed',
          marginLeft: scrollX * -1,
          bottom: 0
        });
      }
      // 通常
      else {
        self.props.$side.css({
          position: 'static',
          marginLeft: 0,
          bottom: 0
        });
      }
    }
  };


  // return class
  return SideNavi;
}());




  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/


}(this, jQuery));
