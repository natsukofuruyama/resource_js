(function(root, $){


  /*----------------------------------------------------------------------
    @constructor
  ----------------------------------------------------------------------*/

  /**
   * googleマップ実装
   *
   * @constructor
   * @class GoogleMap
   * @return {GoogleMap}
   */
  function GoogleMap(target, options){

    this.$window = $(window);
    this.target = target;

    this.props = $.extend(true, {}, {
      lat : [0, 0],
      zoom: 16,
      iconImage: ''
    }, GoogleMap.defaluts, options);

    this.latlng = null;
    this.map = null;
    this.m_latlng1 = null;
    this.marker1 = null;

    this._init();
  }

  var p = GoogleMap.prototype;


  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  /**
   * バージョン情報
   *
   * @static
   * @property VERSION
   * @type {String}
   */
    GoogleMap.VERSION = '1.0';


  /**
   * 定数
   *
   * @property p
   * @type {Object}
   */
    GoogleMap.defaluts = {
    };

  /**
   * プロトタイプオブジェクト
   *
   * @property p
   * @type {Object}
   */
    p = GoogleMap.prototype;

  /**
   * パラメータ
   *
   * @type {Object}
   */
    p.param = null;


  /**
   * クラスネーム
   *
   * @property name
   * @type {String}
   */
    p.name = 'GoogleMap';



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期実行
   */
  p._init = function(){
    var self = this;

    self._set();
    if (self.resize) {
      self._addResizeEvent()
    }
  };


  /**
   * 初期設定
   */
  p._set = function () {
    var self = this;

    self.latlng = new google.maps.LatLng(self.props.lat[0], self.props.lat[1]);

    self.map = new google.maps.Map(document.getElementById(self.target), {
      zoom: self.props.zoom,
      center: self.latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false
    });


    self.m_latlng1 = new google.maps.LatLng(self.props.lat[0], self.props.lat[1]);
    self.marker1 = new google.maps.Marker({
      position: self.m_latlng1,
      map: self.map,
      icon: self.props.iconImage
    });
  };


  /**
   * リサイズイベント追加
   */
  p._addResizeEvent = function () {
    var self = this;

    self.$window.on(resize.gmap, function () {
      self.map.panTo(self.latlng);
    });
  };






  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.GoogleMap = GoogleMap;


}(window, jQuery));
