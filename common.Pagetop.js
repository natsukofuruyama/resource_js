
(function(root, $){


  /**
   *  ページトップ
   *
   * @class Pagetop
   * @constructor
   * @param {jQuery} $pagetop
   * @param {Object} options
   */
  function Pagetop($pagetop) {
    this.$window = $(window);
    this.$pagetop = $pagetop;
    this.showClass = 'is-show';
    this.showFlag = false;

    this._init();
  }

  // prototype
  var p = Pagetop.prototype;



  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  p.className = 'Pagetop';

  /**
   * デフォルト値オブジェクト
   * コンストラクタが呼び出し時に、引数とoptionsをmixinしてpropsオブジェクトに格納します
   *
   * @static
   * @property OPTIONS
   * @type {Object}
   */
  Pagetop.OPTIONS = {
  };



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   *
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function(){
    this._set();
  };



  /**
   * 初期設定
   *
   * @_set
   * @method _set
   * @return {Void}
   */
  p._set = function(){
    this._event();
  };



  /**
   * イベント
   *
   * @set
   * @method set
   * @return {Void}
   */
  p._event = function(){
    var self = this;


    self.$window.on('resize.pagetop scroll.pagetop', function() {
      if (self.$window.height() / 2 < self.$window.scrollTop() && !self.showFlag) {
        self.showFlag = true;
        self.$pagetop.addClass(self.showClass);
      }
      else if (self.$window.scrollTop() < self.$window.height() / 2 && self.showFlag) {
        self.showFlag = false;
        self.$pagetop.removeClass(self.showClass);
      }
    });
  };




  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.Pagetop = Pagetop;

}(this, jQuery));
