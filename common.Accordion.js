
(function(root, $){


  /**
   *  アコーディオン
   *
   * @class Accordion
   * @constructor
   * @param {jQuery} $accordion
   * @param {Object} options
   */
  function Accordion($accordion, options) {
    this.props = $.extend(
      true,
      {},
      this.props,
      Accordion.OPTIONS,
      {
        $trigger: $accordion.find('.acc-btn'),
        $content: $accordion.find('.acc-cont'),
        openClass: 'is-acc-open'
      },
      options
    );

    this._init();
  }

  // prototype
  var p = Accordion.prototype;



  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  p.className = 'Accordion';

  /**
   * デフォルト値オブジェクト
   * コンストラクタが呼び出し時に、引数とoptionsをmixinしてpropsオブジェクトに格納します
   *
   * @static
   * @property OPTIONS
   * @type {Object}
   */
  Accordion.OPTIONS = {
  };



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   *
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function(){
    this._set();
  };



  /**
   * 初期設定
   *
   * @_set
   * @method _set
   * @return {Void}
   */
  p._set = function(){
    this._event();
  };



  /**
   * イベント
   *
   * @set
   * @method set
   * @return {Void}
   */
  p._event = function(){
    var self = this;
    self.addEvent();
  };


  p.addEvent = function() {
    var self = this;
    self.props.$trigger.on('click.Accordion', function() {
      if ($(this).hasClass(self.props.openClass)) {
        self._close($(this));
      } else {
        self._open($(this));
      }

      return false;
    });
  };


  p.removeEvent = function() {
    var self = this;
    self.props.$trigger.off('click.Accordion');
  };


  p._open = function($trigger) {
    var self = this;
    $trigger.addClass(self.props.openClass);
    $trigger.next(self.props.$content).addClass(self.props.openClass);
  };


  p._close = function($trigger) {
    var self = this;
    $trigger.removeClass(self.props.openClass);
    $trigger.next(self.props.$content).removeClass(self.props.openClass);
  };




  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.Accordion = Accordion;

}(this, jQuery));
