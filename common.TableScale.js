(function(root, $){


  /*----------------------------------------------------------------------
    @constructor
  ----------------------------------------------------------------------*/

  /**
   * テーブルをスマホ時スケールさせ、
   * PC時は標準サイズで表示します
   *
   * @constructor
   * @class TableScale
   * @return {TableScale}
   */
  function TableScale($target, $wrap, options){

    this.$target = $target;

    this.props = $.extend(true, {}, {
      $window     : $(window),
      $target     : $target,
      $wrap       : $wrap,
      adjust      : 40,
      isMediaQuery: ''
    }, TableScale.defaluts, options);

    this._init();
  }

  var p = TableScale.prototype;


  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  /**
   * バージョン情報
   *
   * @static
   * @property VERSION
   * @type {String}
   */
    TableScale.VERSION = '1.0';


  /**
   * 定数
   *
   * @property p
   * @type {Object}
   */
    TableScale.defaluts = {
    };

  /**
   * プロトタイプオブジェクト
   *
   * @property p
   * @type {Object}
   */
    p = TableScale.prototype;

  /**
   * パラメータ
   *
   * @type {Object}
   */
    p.param = null;


  /**
   * クラスネーム
   *
   * @property name
   * @type {String}
   */
    p.name = 'TableScale';



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期実行
   */
  p._init = function(){
    this._onEvent();

    if (AMP.mediaquery.mediaStyle === 'sp') {
      this._spSize();
    } else {
      this._pcSize();
    }

    setTimeout(function() {
      $(window).trigger('resizestop.tableScale');
    }, 1000);
  };

  /**
   * イベント
   */
  p._onEvent = function(){
    var self = this;

    self.props.isMediaQuery = AMP.mediaquery.mediaStyle;
    self.props.$window.on('resizestop.tableScale', function() {
      if (AMP.mediaquery.mediaStyle === 'sp') {
        self._spSize();
      } else {
        self._pcSize();
      }
    });
  };

  /**
   * スマホサイズ
   */
  p._spSize = function() {
    common.util.scaleSize(this.props.$target, this.props.$wrap);
    this.props.isMediaQuery = AMP.mediaquery.mediaStyle;
  };

  /**
   * PCサイズ
   */
  p._pcSize = function() {
    if (this.props.isMediaQuery === 'sp') {
      this.props.$wrap.removeAttr('style');
      this.props.isMediaQuery = AMP.mediaquery.mediaStyle;
    }
  };



  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.TableScale = TableScale;


}(window, jQuery));
