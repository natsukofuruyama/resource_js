(function(root, $){


  /*----------------------------------------------------------------------
    @constructor
  ----------------------------------------------------------------------*/

  /**
   * Modal実装
   *
   * @constructor
   * @class Modal
   * @return {Modal}
   */
  function Modal(options){
    this.props = $.extend(
      true,
      {},
      {
        $elementWrap        : $('#modal-elements'),
        TRIGGER_CLASS       : 'modal-trigger',
        TRIGGER_CLOSE_CLASS : 'modal-trigger-close',
        ANCHOR_CLASS        : 'modal-anchor',
        // 表示させるモーダル
        dataModalId         : 'data-modal-id',
        // モーダル内でスクロール位置
        dataModalPos        : 'data-modal-pos',
        modalBg             : '<div id="modal-bg" class="modal-bg"><div></div></div>',
        modalFrame          : '<div id="modal" class="modal">\n<div class="modal-frame">\n<p class="modal-close-btn"><a href="#" class="modal-trigger-close"><span class="modal-trigger-close__line modal-trigger-close__line--01"></span><span class="modal-trigger-close__line modal-trigger-close__line--02"></span></a></p>\n<div class="modal-container">\n<div class="modal-content">\n</div>\n</div>\n</div>\n</div>',
        duration            : 100,
        easing              : 'easeOutQuad',
        $fixedDom           : $(''),

        queryScroll: {
          query        : 'anchor',
          adjust       : 0,
          tween        : {
            mobileHA   : false,
            duration   : 800,
            easing     : 'easeOutQuint'
          }
        }
      },
      Modal.defaluts,
      options
    );

    // this._init();
  }

  var p = Modal.prototype;


  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  /**
   * バージョン情報
   *
   * @static
   * @property VERSION
   * @type {String}
   */
    Modal.VERSION = '1.0';


  /**
   * 定数
   *
   * @property p
   * @type {Object}
   */
    Modal.defaluts = {
      $body               : $('body'),
      $html               : $('html'),
      $win                : $(window),
      $doc                : $(document),

      $modal              : null,
      $bg                 : null,
      $content            : null,
      $container          : null,
      $cacheElement       : null,

      scrollbarWidth      : window.innerWidth - document.body.clientWidth,

      isAnime             : false,
      isOpen              : false,
      OPEN_CLASS          : 'is-modal-open'
    };

  /**
   * プロトタイプオブジェクト
   *
   * @property p
   * @type {Object}
   */
    p = Modal.prototype;

  /**
   * パラメータ
   *
   * @type {Object}
   */
    p.props = null;


  /**
   * クラスネーム
   *
   * @property name
   * @type {String}
   */
    p.name = 'Modal';



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期実行
   *
   * @public
   * @return {Void}
   */
  p.init = function(){
    var self = this;

    if(!self.props.$body.find('#modal').length) {
      self.props.$body.append(self.props.modalFrame);
      self.props.$body.prepend(self.props.modalBg);

      self.props.$modal = self.props.$body.find('#modal');
      self.props.$bg = self.props.$body.find('#modal-bg');
      self.props.$container = self.props.$body.find('#modal .modal-container');
      self.props.$content = self.props.$body.find('#modal .modal-content');
      self.props.$frame = self.props.$body.find('#modal .modal-frame');
    }

    $('a[class="'+ self.props.TRIGGER_CLASS +'"]').off('click');

    self._event();
  };


  /**
   * イベント
   *
   * @private
   * @return {Void}
   */
  p._event = function(){
    var self = this;
    // オープンボタン
    self.props.$doc.on('click.commonModal', 'a[class~="'+ self.props.TRIGGER_CLASS +'"]', function(event){
      if(self.props.isAnime) return false;

      var modalId = '#' + $(this).attr(self.props.dataModalId);
      var scrollId = $(this).attr(self.props.dataModalPos);

      if (scrollId) {
        scrollId = '#' + scrollId;
      }
      self._controller(modalId, scrollId);
      return false;
    });

    // モーダル内スクロール
    self.props.$doc.off('click.commonModal', 'a[class~="'+ self.props.ANCHOR_CLASS +'"]');
    self.props.$doc.on('click.commonModal', 'a[class~="'+ self.props.ANCHOR_CLASS +'"]', function(event){
      var scrollId = $(this).attr('href');
      self._scrollTween(self.props.$frame.find(scrollId));
      return false;
    });

    // container要素以外
    self.props.$doc.on('click.commonModal', function(event){
      if ($.contains(event.target, self.props.$container[0])) {
        if(self.props.isAnime || !self.props.isOpen) return false;
        self._closeModal();
      }
    });

    // クローズボタン
    self.props.$doc.on('click.commonModal', 'a[class~="'+ self.props.TRIGGER_CLOSE_CLASS +'"]', function(event){
      if(self.props.isAnime || !self.props.isOpen) return false;
      self._closeModal();
      return false;
    });

    self.props.$win.on('scroll.commonModal, resize.commonModal', function(event) {
      self.props.$bg.css({
        left: self.props.$win.scrollLeft() * -1,
      });
      self.props.$modal.css({
        left: self.props.$win.scrollLeft() * -1,
      });

      // モーダル開いていれば
      if (self.props.isOpen) {
        self.props.$html
          .css({ marginRight: 0})
          .css({ marginRight: window.innerWidth - document.body.clientWidth});
        self.props.$fixedDom
          .css({ marginRight: 0})
          .css({ marginRight: window.innerWidth - document.body.clientWidth});
      }
    });
  };


  /**
   * コントロール
   *
   * @param  {Strings} modal 対象のモーダル
   * @param  {Strings} scrollId スクロールさせたい位置のDOMのID
   * @return {Void}
   */
  p._controller = function(modal, scrollId){
    var self = this;

    // モーダルが開いている場合
    if (self.props.isOpen) {
      $.sequence(
        function() {
          return self._changeHideAnime();
        },
        function(){
          self._addContent(modal);
          self._changeShowAnime(scrollId);
        }
      );
    }

    // 通常オープン
    else {
      self._addContent(modal);
      self._showModal(scrollId);
    }
  };


  /**
   * コンテンツ入れ替え
   *
   * @private
   * @param  string modal 対象のid
   * @return {Void}
   */
  p._addContent = function(modal){
    var self = this;

    self.props.$cacheElement = self.props.$body.find(modal).detach();
    self.props.$content.css({opacity: 0});
    self.props.$content.append(self.props.$cacheElement);

    return self.props.$body.delay(100);
  };


  /**
   * オープン時のコールバック
   *
   * @private
   * @return {Void}
   */
  p._openModalFunc = function() {
  };


  /**
   * オープン後のコールバック
   *
   * @private
   * @return {Void}
   */
  p._openedModalFunc = function() {
  };


  /**
   * クローズ後のコールバック
   *
   * @private
   * @return {Void}
   */
  p._closeModalFunc = function() {
  };
  /**
   * クローズ後のコールバック
   *
   * @private
   * @return {Void}
   */
  p._closedModalFunc = function() {
  };


  /**
   * モーダルオープン
   *
   * @private
   * @param  {Strings} scrollId スクロールさせたい位置のDOMのID
   * @return {Void}
   */
  p._showModal = function(scrollId){
    var self = this;

    // フラグ
    self.props.isAnime = true;
    self.props.isOpen = true;

    // コールバック
    self._openModalFunc();

    // アニメーションfrom
    self.props.$content.css({
      opacity: 0
    });
    self.props.$bg.css({
      opacity: 0
    });

    // モーダル表示クラス
    self.props.$html
      .css({ marginRight: 0})
      .css({ marginRight: window.innerWidth - document.body.clientWidth})
      .addClass(self.props.OPEN_CLASS);
    self.props.$fixedDom
      .css({ marginRight: 0})
      .css({ marginRight: window.innerWidth - document.body.clientWidth});
    self.props.$bg.addClass(self.props.OPEN_CLASS);
    self.props.$modal.addClass(self.props.OPEN_CLASS);


    // スクロール位置リセット
    self.props.$frame.scrollTop(0);


    // モーダル表示アニメーション
    self.props.$bg
      .velocity({
        opacity: 1
      }, self.props.duration, self.props.ease);
    self.props.$content
      .velocity({
        opacity: 1
      }, self.props.duration, self.props.ease,
       function() {
        self._openedModalFunc();

        // スクロール
        if (scrollId) {
          self._anchorScroll(scrollId);
        }

        self.props.isAnime = false;
       });
  };


  /**
   * モーダルクローズ
   *
   * @private
   * @return {Void}
   */
  p._closeModal = function(){
    var self = this;


    self.props.isAnime = true;

    // コールバック
    self._closeModalFunc();

    self.props.$bg
      .velocity({
        opacity: 0
      }, self.props.duration, self.props.ease);
    self.props.$content
      .velocity({
        opacity: 0
      }, self.props.duration, self.props.ease,
        function(){
          self._closedModalFunc();

          self.props.$elementWrap.append(self.props.$cacheElement);
          self.props.$cacheElement = null;
          self.props.isAnime = false;
          self.props.isOpen = false;
          self.props.$content.empty();
          self.props.$modal.removeClass(self.props.OPEN_CLASS);
          self.props.$bg.removeClass(self.props.OPEN_CLASS);
          self.props.$html
            .removeClass(self.props.OPEN_CLASS)
            .css({
              marginRight: 0
            });
          self.props.$fixedDom
            .css({
              marginRight: 0
            });
        });
  };


  /**
   * モーダル切り替えオープンアニメーション
   *
   * @private
   * @param  {Strings} scrollId スクロールさせたい位置のDOMのID
   * @return {Object}
   */
  p._changeShowAnime = function(scrollId) {
    var self = this;

    self.props.isAnime = true;

    self.props.$frame.scrollTop(0);
    self._openModalFunc();

    self.props.$content.css({
      opacity: 0
    });

    self.props.$content
      .velocity({
        opacity: 1
      }, self.props.duration, self.props.ease,
       function() {
        self._openedModalFunc();
        self.props.isAnime = false;
        if (scrollId) {
          self._anchorScroll(scrollId);
        }
       });

    return self.props.$body.delay(self.props.duration);
  };


  /**
   * モーダル切り替えクローズアニメーション
   *
   * @private
   * @return {Object}
   */
  p._changeHideAnime = function() {
    var self = this;

    self.props.isAnime = true;
    return  self.props.$content
      .velocity({
        opacity: 0
      }, self.props.duration, self.props.ease, self.props.duration, self.props.ease,
        function(){
          self._closedModalFunc();

          self.props.$elementWrap.append(self.props.$cacheElement);
          self.props.$cacheElement = null;
          self.props.$content.empty();
          return self.props.$body;
        });

    // return self.props.$body.delay(1000);
  };




  /**
   * スクロールアニメーション
   * @param  {Strings} scrollId スクロールさせたい位置のDOMのID
   */
  p._anchorScroll = function(scrollId){
    var self = this;

    self.props.$frame.imagesLoaded(function() {
      if (0 < self.props.$frame.find(scrollId).length) {
        self._scrollTween(self.props.$frame.find(scrollId));
      }
    });
  };



  /**
   * スクロールアニメーション
   * @param  {Strings} scrollId スクロールさせたい位置のDOMのID
   */
  p._scrollTween = function($target){
    var self = this;

    var param = $.extend({container: self.props.$frame}, self.props.queryScroll.tween);
    $target.velocity('scroll', param);
  };


  /**
   * オープン
   *
   * @param  {Strings} modal 対象のモーダル
   * @return {Void}
   */
  p.open = function(modal) {
    this._controller(modal);
  };


  /**
   * クローズ
   *
   * @return {Void}
   */
  p.close = function() {
    this._closeModal();
  };


  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.Modal = Modal;


}(window, jQuery));
