
(function(root, $){


  /**
   *  ２列～の表示スライダー
   *
   * @class QueryScroll
   * @constructor
   * @param {jQuery} $slider
   * @param {Object} options
   */
  function QueryScroll(options) {
    this.props = $.extend(
      true,
      {},
      this.props,
      QueryScroll.OPTIONS,
      {
        $html        : $('html, body'),
        query        : 'anchor',
        adjust       : 0,
        tween        : {
          mobileHA   : false,
          duration   : 800,
          easing     : 'easeOutQuint'
        }
      },
      options
    );

    this._init();
  }

  // prototype
  var p = QueryScroll.prototype;



  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  p.className = 'QueryScroll';

  /**
   * デフォルト値オブジェクト
   * コンストラクタが呼び出し時に、引数とoptionsをmixinしてpropsオブジェクトに格納します
   *
   * @static
   * @property OPTIONS
   * @type {Object}
   */
  QueryScroll.OPTIONS = {
  };



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   *
   * @private
   * @method _init
   * @return {Void}
   */
  p._init = function(){
    var self = this;

    $('body').imagesLoaded(function() {
      var $anchor = $('#' + common.util.getQueryVariable(self.props.query));
      if ($anchor.length) {
        self.tween(self.props.$html, $anchor.offset().top);
      }
    });
  };


  /**
   * スクロールアニメーション
   */
  p.tween = function($target, moveTo){
    var self = this;

    moveTo = moveTo - self.props.adjust;

    var param = $.extend({offset: moveTo}, self.props.tween);

    $('html, body').velocity('stop').velocity('scroll', param);
  };



  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.QueryScroll = QueryScroll;

}(this, jQuery));
