
(function(root, $){


  /**
   *  スライダー
   *
   * @class Slider
   * @constructor
   * @param {jQuery} $slider
   * @param {Object} options
   */
  function Slider($slider, options) {
    this.props = $.extend(
      true,
      {},
      this.props,
      Slider.OPTIONS,
      {
        $window     : $(window),
        $slider     : $slider.find('.slider'),
        $stage      : $slider.find('.stage'),
        $list       : $slider.find('.list'),
        $item       : $slider.find('.list').children(),
        $prev       : $slider.find('.prev a'),
        $next       : $slider.find('.next a'),
        $pointer    : $slider.find('.pointer'),
        $thumbnail  : $slider.find('.thumbnail').children(),
        $point      : $slider.find('.pointer').children(),
        current     : 0,
        timer       : 5000,
        CLONE       : 3,
        infinite    : true,
        responsive  : false,

        ease        : 'easeOutExpo',
        duration    : 600
      },
      options
    );

    this.props.itemLength = this.props.$item.length;
  }

  // prototype
  var p = Slider.prototype;



  /*--------------------------------------------------------------------------
    @property
  --------------------------------------------------------------------------*/

  p.className = 'Slider';

  /**
   * デフォルト値オブジェクト
   * コンストラクタが呼び出し時に、引数とoptionsをmixinしてpropsオブジェクトに格納します
   *
   * @static
   * @property OPTIONS
   * @type {Object}
   */
  Slider.OPTIONS = {
    next        : 0,
    timerId     : null,
    itemLength  : 0,
    ACTIVE_CLASS: 'is-active',
    _isAnime    : false
  };



  /*--------------------------------------------------------------------------
    @method
  --------------------------------------------------------------------------*/

  /**
   * 初期化
   *
   * @private
   * @method _init
   * @return {Void}
   */
  p.init = function(){
    var self = this;
    self.props.$stage.imagesLoaded(function(){
      self._set();
    });
  };



  /*--------------------------------------------------------------------------
     Setting
  ---------------------------------------------------------------------------*/

  /**
   * 初期設定
   *
   * @_set
   * @method _set
   * @return {Void}
   */
  p._set = function(){
    var self = this;

    self.setStyle();
    self.setPoint();
    self.setItemClone();
    self.setTimer();
    self._event();
    self._flickEvent();


    // レスポンシブ
    if (self.props.responsive) {
      self._setResponsiveStyle();
      self._resizeEvent();
    }
    // 無限ループ
    if (self.props.infinite) {
      self._setLoopStyle();
      self.tween = self._loopTween;
    }
    // ループなし
    else {
      self._setNotLoopStyle();
      self.tween = self._notLoopTween;
    }
  };

  /**
   * ポインターセット
   *
   * @setPoint
   * @method setPoint
   * @return {Void}
   */
  p.setPoint = function(){
    var self = this;
    var i = 0,
        c = '';
    for(; i < self.props.itemLength; i++){
      c += self.props.$pointer.html();
    }
    self.props.$pointer.html(c);
    self.props.$point = self.props.$pointer.find('li a');
    self.props.$point.eq(self.props.current).addClass(self.props.ACTIVE_CLASS);
  };

  /**
   * アイテムクローン
   *
   * @setItemClone
   * @method setItemClone
   * @return {Void}
   */
  p.setItemClone = function(){
    var self = this;

    if (self.props.infinite) {
      var i = 0,
          c = '';

      for(; i < self.props.CLONE; i++){
        c += self.props.$list.html();
      }
      self.props.$list.html(c);
      self.props.$item = self.props.$list.children();
    }
  };

  /**
   * 初期スタイル
   *
   * @setStyle
   * @method setStyle
   * @return {Void}
   */
  p.setStyle = function(){
    var self = this;

    var i = self.props.$item.slice(0, self.props.current);
    self.props.$list.slice(0, self.props.current).append(i);

    self.props.$thumbnail.eq(self.props.current).addClass(self.props.ACTIVE_CLASS);
  };


  /**
   * スライダーのポジションのセット
   *
   * @setStyle
   * @method setStyle
   * @return {Void}
   */
  p.setSliderPosition = function () {
    var self = this;

    // 無限ループ
    if (self.props.infinite) {
      self.props.$list.css({
        width: self.props.$stage.width() * self.props.itemLength * self.props.CLONE,
        left: self.props.$stage.width() * self.props.itemLength * -1
      });
    }
    // ループなし
    else {
      self.props.$list.css({
        width: self.props.$stage.width() * self.props.itemLength
      });
    }
  };

  /**
   * タイマーセット
   *
   * @setTimer
   * @method setTimer
   * @return {Void}
   */
  p.setTimer = function(){
    var self = this;
    clearTimeout(self.props.timerId);
    if(self.props.timer){
      self.props.timerId = setTimeout(function(){
        self._nextEvent();
      }, self.props.timer);
    }
  };

  /**
   * スライダーのスタイル
   * リセット
   *
   * @_resetStyle
   * @method _resetStyle
   * @return {Void}
   */
  p._resetStyle = function () {
    clearTimeout(self.props.timerId);

    self.props.$stage.removeAttr('style');
    self.props.$item.removeAttr('style');
  };

  /**
   * 幅変動のあるスライダー
   *
   * @_setResponsiveStyle
   * @method _setResponsiveStyle
   * @return {Void}
   */
  p._setResponsiveStyle = function(){
    var self = this;

    clearTimeout(self.props.timerId);

    // スライダーのアイテムが段落ちしないようにするため
    // 一度stageに幅の倍の値をwidthに当てる
    self.props.$list.css({
      width: self.props.$list.width() * 2
    });
    self.props.$item.css({
      width: self.props.$stage.width()
    });
    self.props.$list.css({
      width: self.props.$item.width() * self.props.itemLength * self.props.CLONE
    });
    self.props.$stage.css({
      height: self.props.$list.height()
    });

    self.setTimer();
  };

  /**
   * 無限ループ
   * スライダーのセット
   *
   * @_setLoopStyle
   * @method _setLoopStyle
   * @return {Void}
   */
  p._setLoopStyle = function(){
    var self = this;

    clearTimeout(self.props.timerId);

    self.props.$list.css({
      width: self.props.$stage.width() * self.props.itemLength * self.props.CLONE,
      left: self.props.$stage.width() * (self.props.itemLength + self.props.current) * -1
    });

    self.setTimer();
  };

  /**
   * ループなし
   * スライダーのセット
   *
   * @_setLoopStyle
   * @method _setLoopStyle
   * @return {Void}
   */
  p._setNotLoopStyle = function(){
    var self = this;

    clearTimeout(self.props.timerId);

    self.props.$list.css({
      width: self.props.$stage.width() * self.props.itemLength,
      left: self.props.$stage.width() * self.props.current * -1
    });

    self.setTimer();
  };


  /**
   * レスポンシブ対応がある場合
   * @return {[type]} [description]
   */
  p.setResponsive = function() {
    this.props.responsive = true;
    this._resetStyle();
    this._setResponsiveStyle();

    // 無限ループ
    if (this.props.infinite) {
      this._setLoopStyle();
    }
    // ループなし
    else {
      this._setNotLoopStyle();
    }
  };


  /**
   * レスポンシブ対応が無い場合
   * @return {[type]} [description]
   */
  p.removeResponsive = function() {
    this.props.responsive = false;
    self._resetStyle();

    // 無限ループ
    if (this.props.infinite) {
      this._setLoopStyle();
    }
    // ループなし
    else {
      this._setNotLoopStyle();
    }
  };



  /*--------------------------------------------------------------------------
     Event
  ---------------------------------------------------------------------------*/

  /**
   * イベント
   *
   * @_event
   * @method _event
   * @return {Void}
   */
  p._event = function(){
    var self = this;

    // prev
    self.props.$prev.click(function(){
      if(self.props._isAnime) return false;
      self._prevEvent();
      return false;
    });

    // next
    self.props.$next.click(function(){
      if(self.props._isAnime) return false;
      self._nextEvent();
      return false;
    });

    // pointer
    self.props.$pointer.find('a').click(function(){
      if(self.props._isAnime) return false;
      self._pointerEvent(self.props.$pointer.find('a').index(this));
      return false;
    });

    // thumbnail
    self.props.$thumbnail.find('a').click(function(){
      if(self.props._isAnime) return false;
      self._pointerEvent(self.props.$thumbnail.find('a').index(this));
      return false;
    });

    // hover
    self.props.$stage.hover(
      function(){
        clearTimeout(self.props.timerId);
      },
      function(){
        self.setTimer();
      });
  };

  /**
   * リサイズイベント
   *
   * @_resizeEvent
   * @method _resizeEvent
   * @return {Void}
   */
  p._resizeEvent = function() {
    var self = this;

    self.props.$window.on('resizestop', function() {
      if (self.props.responsive) {
        self._setResponsiveStyle();
        self.setSliderPosition();
      }
    });
  };

  /**
   * PREVイベント
   *
   * @_prevEvent
   * @method _prevEvent
   * @return {Void}
   */
  p._prevEvent = function(){
    var self = this;
    self._controlCount(self.props.current - 1);
    self._viewStart(-1);
  };

  /**
   * NEXTイベント
   *
   * @_nextEvent
   * @method _nextEvent
   * @return {Void}
   */
  p._nextEvent = function(){
    var self = this;
    self._controlCount(self.props.current + 1);
    self._viewStart(1);
  };

  /**
   * ポインターイベント
   *
   * @_pointerEvent
   * @method _pointerEvent
   * @return {Void}
   */
  p._pointerEvent = function(index){
    var self = this;

    var d, // デフォルト移動 1:next -1: prev
        p, // next移動
        m; // prev移動

    if(self.props.current < index){
      d = 1;
      p = index - self.props.current;
      m = self.props.itemLength - p;
    } else {
      d = -1;
      m = self.props.current - index;
      p = self.props.itemLength - m;
    }

    self._controlCount(index);
    self._viewStart(self._controlPointer(d, p, m));
  };

  /**
   * フリックイベント
   *
   * @_flickEvent
   * @method _flickEvent
   * @return {Void}
   */
  p._flickEvent = function(){
    var self = this;

    self.props.$stage
    .on('flickmoveX', function(event){
      if(self.props._isAnime) return false;
      clearTimeout(self.props.timerId);
      self.props.$list.css({marginLeft: event.moveX});
    })
    .on('flickcancel', function(event){
      if(self.props._isAnime) return false;
      self.props.$list.stop(true, false).velocity({marginLeft: 0}, self.props.duration, self.props.ease,
        function(){
          self.setTimer();
        });
    })
    .on('flick', function(event){
      if(event.moveX < 0 && !self.props._isAnime){
        self._nextEvent();
      }
      else if (0 < event.moveX && !self.props._isAnime) {
        self._prevEvent();
      }
      else if(!self.props._isAnime){
        self.props.$list.stop(true, false).velocity({marginLeft: 0}, self.props.duration, self.props.ease,
          function(){
            self.setTimer();
          });
      }
    });
  };



  /*--------------------------------------------------------------------------
     Control
  ---------------------------------------------------------------------------*/

  /**
   * ポインターコントロール
   *
   * @_controlPointer
   * @method _controlPointer
   * @return {Number}
   */
  p._controlPointer = function(d, p, m){
    var self = this;
    if(m < p) {
      return m * -1;
    }
    if(p < m) {
      return p;
    }
    if(p === m) {
      return p * d;
    }
  };

  /**
   * nextカウント
   *
   * @_controlCount
   * @method _controlCount
   * @return {Void}
   */
  p._controlCount = function(next){
    var self = this;
    if(next == self.props.itemLength){
      self.props.next = 0;
    } else if (next < 0) {
      self.props.next = self.props.itemLength -1;
    } else {
      self.props.next = next;
    }
  };

  /**
   * ビューに投げる前
   *
   * @_viewStart
   * @method _controlPointer
   * @return {Void}
   */
  p._viewStart = function(distance){
    var self = this;
    clearTimeout(self.props.timerId);
    self.props._isAnime = true;
    self.pointer(self.props.current, self.props.next);
    self.tween(distance);
  };



  /*--------------------------------------------------------------------------
     View
  ---------------------------------------------------------------------------*/

  /**
   * ポインターチェンジ
   *
   * @pointer
   * @method pointer
   * @return {Void}
   */
  p.pointer = function(current, next){
    var self = this;
    self.props.$point.eq(current).removeClass(self.props.ACTIVE_CLASS);
    self.props.$point.eq(next).addClass(self.props.ACTIVE_CLASS);
    self.props.$thumbnail.eq(current).removeClass(self.props.ACTIVE_CLASS);
    self.props.$thumbnail.eq(next).addClass(self.props.ACTIVE_CLASS);
  };

  /**
   * スライドアニメーション
   *
   * @tween
   * @method tween
   * @return {Void}
   */
  p.tween = function(distance){

  };

  /**
   * ループ
   * スライダーアニメーション
   *
   * @_loopTween
   * @method _loopTween
   * @return {Void}
   */
  p._loopTween = function (distance) {
    var self = this;

    self.props.$list.stop(true, false).velocity({marginLeft: self.props.$stage.width() * distance * -1}, self.props.duration, self.props.ease,
      function(){
        var $self = $(this);
        var clone;
        if(0 < distance){
          $self.css({
            marginLeft: 0
          }).append($self.find('li').slice(0, distance));
        } else {
          $self.css({
            marginLeft: 0
          }).prepend($self.find('li').slice(self.props.itemLength * self.props.CLONE + distance, self.props.itemLength * self.props.CLONE));
        }

        self.props.current = self.props.next;
        self.props._isAnime = false;
        self.setTimer();
      });
  };

  /**
   * スライダーアニメーション
   *
   * @_notLoopTween
   * @method _notLoopTween
   * @return {Void}
   */
  p._notLoopTween = function (distance) {
    var self = this;

    var next = self.props.current + distance;
    if (next === self.props.itemLength) {
      next = 0;
    } else if (next < 0) {
      next = self.props.itemLength - 1;
    }

    self.props.$list
      .stop(true, false)
      .velocity({
        left: self.props.$stage.width() * next * -1,
        marginLeft: 0
      }, self.props.duration, self.props.ease,
        function(){
          self.props.current = next;
          self.props._isAnime = false;
          self.setTimer();
        });
  };




  /*--------------------------------------------------------------------------
    export
  --------------------------------------------------------------------------*/

  root.common = root.common || {};
  common.Slider = Slider;

}(this, jQuery));
