カルーセルやモーダルなど、実装のベースになるソースを置いてます。  
（案件に応じて適宜調整をする前提）  
基本レスポンシブ対応済で、そのままソースを入れても動きます。  
自分のメモのつもりなのでしっかり作り込むつもりは今のところないです！



## 現在までのリソース
common.util.jsを読み込んだ上で機能するファイルもあります。


- **common.Modal**  
	モーダル  
	CSSファイルも読み込ませてください。  

- **common.Accordion.js**  
	アコーディオン  

- **common.ColumnsSlider.js**  
	スライダー  
	リサイズ時にアイテム表示の数が変動した場合も再計算します。  

- **common.GlobalNavi.js**  
	グローバルナビ  
	html・css構造も関わるのでそのままでは使いにくいです。  
	SPになったときに格納ナビに切り替わります。  

- **common.GoogleMap.js**  
	Google Maps APIの実装を楽にします。  

- **common.Pagetop.js**  
	ページTOPボタンクリック時にスクロールアニメーション  
	表示非表示のトリガーになるクラスを振ります。

- **common.QueryScroll.js**  
	 URLに対象のクエリがある場合に目的の位置までスクロールさせます。

- **common.SideNavi.js**  
	スクロール時にサイドナビを固定させます。

- **common.Slider.js**  
	スライダー  
	カラム数の切り替えができます。

- **common.Tab.js**  
	タブ

- **common.TableScale.js**  
	テーブルのスマホ最適化  
	スマホの横幅に合わせてテーブル全体をスケールさせることができます。

- **common.YoutubePlayer.js**  
	YouTube Data APIの実装を楽にします。

- **common.YoutubePlayerFullsize.js**  
	Youtubeを全画面で表示させたいときに使います。

- **common.util.js**  
	便利機能  
	メディアクエリの切り替わりの監視やクエリの取得などがあります。





## 依存しているライブラリ
- [jQuery](https://jquery.com/)
- [Underscore.js](http://underscorejs.org/)
- [Velocity.js](http://velocityjs.org/)
- [imagesLoaded](http://imagesloaded.desandro.com/)



## 対応ブラウザ
- Chrome
- Firefox
- Safari(Mac)
- IE10〜
- Edge
- Android 4.4〜
- iOS7〜
